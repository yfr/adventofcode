var overlaps = [];
var inches = 0;

areas.forEach(area => {
	let [id, at, coords, square] = area.replace('@', '').replace(':', '').split(' ');
console.log(id)
	let [x, y] = coords.split(',');
	let [columns, rows] = square.split('x');

	x = Number(x);
	y = Number(y);
	columns = Number(columns);
	rows = Number(rows);
	for (r=y; r<=rows+y-1; r++){
		
        if (!overlaps[r]) {
            overlaps[r] =[];
        }
		for (c=x; c<=columns+x-1; c++){
		if (overlaps[r][c]){
			overlaps[r][c]++;}
			else {
			overlaps[r][c] = 1}
    	}
    }

})

console.log(overlaps)

overlaps.forEach(row => {
    row.forEach(c => {
        if (c && c>=2) {
            inches++
        }
    })
})


console.log(inches)


areas.forEach(area => {
	let doesOverlap = false;
	let [id, at, coords, square] = area.replace('@', '').replace(':', '').split(' ');
	let [x, y] = coords.split(',');
	let [columns, rows] = square.split('x');

	x = Number(x);
	y = Number(y);
	columns = Number(columns);
	rows = Number(rows);

	for (r=y; r<=rows+y-1; r++){
		for (c=x; c<=columns+x-1; c++){
            if (overlaps[r][c] > 1){
                doesOverlap = true
            }
    	}
    }

	if (!doesOverlap) {
		console.log({area});
    }

})
