// 400 players; last marble is worth 71864 points
let marble = 1;
let game = [0];
let currentMarblePosition = 0;
let points = {};

let players = 400;
let marbles = 71864;

while (marble < marbles) {
  if (marble % 23 === 0) {
    const player = (marble - 1) % players;
    const removePosition = (currentMarblePosition =
      ((game.length + currentMarblePosition - 8) % game.length) + 1);

    const removedMarble = game.splice(removePosition, 1)[0];

    if (!points[player]) {
      points[player] = 0;
    }

    points[player] += marble + removedMarble;

    currentMarblePosition = removePosition;

    marble++;
    continue;
  }

  currentMarblePosition =
    ((game.length + currentMarblePosition + 1) % game.length) + 1;

  game.splice(currentMarblePosition, 0, marble);

  marble++;
}

const sortable = [];
for (var key in points) {
  sortable.push([key, points[key]]);
}
console.log(sortable.sort(([, a], [, b]) => b - a)[0]);
