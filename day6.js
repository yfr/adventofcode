let input = require('./day6-input');

input = input.map((i, index) => {
  i[2] = index;

  return i;
});

const findHighestX = input => {
  return input.sort((a, b) => {
    return b[0] - a[0];
  })[0][0];
};
const findHighestY = input => {
  return input.sort((a, b) => {
    return b[1] - a[1];
  })[0][1];
};

const findClosestStation = (x, y, points) => {
  const mSorted = points
    .map(point => {
      point[3] = mdistance(x, y, point[0], point[1]);
      return point;
    })
    .sort((a, b) => {
      return a[3] - b[3];
    });

  if (mSorted[0][3] === mSorted[1][3]) {
    return '.';
  }

  return mSorted[0][2];
};

const mdistance = (x1, y1, x2, y2) => {
  return Math.abs(x2 - x1) + Math.abs(y2 - y1);
};

const maxX = findHighestX(input);
const maxY = findHighestY(input);

const distanceSum = (x, y, points) => {
  return points
    .map(point => {
      point[3] = mdistance(x, y, point[0], point[1]);
      return point;
    })
    .reduce((d, p) => {
      return d + p[3];
    }, 0);
};

const distances = [];
for (let y = 0; y <= maxY; y++) {
  for (let x = 0; x <= maxX; x++) {
    const distance = distanceSum(x, y, input);
    if (distance < 10000) {
      distances.push(distanceSum(x, y, input));
    }
  }
}

console.log(distances.length);

const findStationWithgreatestArea = () => {
  const closest = [];
  for (let y = 0; y <= maxY; y++) {
    for (let x = 0; x <= maxX; x++) {
      if (!closest[y]) {
        closest[y] = [];
      }
      closest[y][x] = findClosestStation(x, y, input);
    }
  }

  const i = [];
  closest[0].forEach(c => i.push(c));
  closest.forEach(c => i.push(c[0]));
  closest[maxY].forEach(c => i.push(c));
  closest.forEach(c => i.push(c[maxX]));

  const area = [];

  closest.forEach((x, xi) => {
    x.forEach((y, yi) => {
      if (!area[closest[xi][yi]]) {
        area[closest[xi][yi]] = 0;
      }
      area[closest[xi][yi]]++;
    });
  });

  area.forEach((size, index) => {
    if (!i.includes(index)) {
      console.log({size, index});
    }
  });
};
