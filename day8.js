let input = require('./day8-input');

input = input.split(' ');
let id = 0;
let metaS = [];

const findBranch = () => {
  const result = [];
  while (true) {
    let [childrenCount, metaEntriesCount] = input.splice(0, 2);

    result.push({
      id,
      childrenCount: Number(childrenCount),
      metaEntriesCount: Number(metaEntriesCount)
    });
    id++;
    if (childrenCount === '0') {
      return result.reverse();
    }
  }
};

const calcNodeSum = node => {
  if (node.childrenCount === 0) {
    return node.meta.reduce((a, b) => a + Number(b), 0);
  }

  return node.meta.reduce((sum, m) => {
    const child = node.children[Number(m) - 1];
    if (!child) {
      return sum;
    }

    if (child) {
      return sum + child.metaSum;
    }
    return sum;
  }, 0);
};

const traverse = branch => {
  return branch.map((node, i, a) => {
    node.children = [];

    // node with only one child and this child is calc already
    if (node.childrenCount === 1 && i === 1) {
      node.children.push(branch[0]);
    }

    if (node.childrenCount > node.children.length) {
      node.children.push(branch[i - 1]);

      while (node.childrenCount > node.children.length) {
        const childBranch = findBranch();
        node.children.push(childBranch[childBranch.length - 1]);

        traverse(childBranch);
      }
    }
    node.meta = input.splice(0, node.metaEntriesCount);

    node.metaSum = calcNodeSum(node);
    metaS = [...metaS, ...node.meta];

    return node;
  });
};

const firstBranch = findBranch();
const tree = traverse(firstBranch);

console.log(tree);
console.log('1: ', tree.find(t => t.id === 0).metaSum);
console.log('2: ', metaS.reduce((a, b) => a + Number(b), 0));
