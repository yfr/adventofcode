const fs = require('fs');
const directions = ['^', '>', 'v', '<'];

const map = fs
  .readFileSync('./day13-input.txt', 'utf8')
  .split('\n')
  .map(line => [...line]);

let carId = 0;
cars = map.reduce((c, line, y) => {
  line.forEach((f, x) => {
    if (directions.includes(f)) {
      map[y][x] = '<>'.includes(f) ? '-' : '|';
      c.push({
        id: carId,
        x,
        y,
        direction: f,
        turn: 0
      });
      carId++;
    }
  });

  return c;
}, []);

const collisionCheck = car => {
  if (crashCars.includes(car.id)) {
    return;
  }
  cars.forEach(c => {
    if (c.id !== car.id && c.x === car.x && c.y === car.y) {
      crashCars = crashCars.concat([c.id, car.id]);

      crash = true;
    }
  });
};

const getDirection = (currentDirection, turn) => {
  const currentDirectionIndex = directions.indexOf(currentDirection);
  const nextDirectionIndex = Math.abs((4 + currentDirectionIndex + turn) % 4);

  return directions[nextDirectionIndex];
};

const getNewDirection = (field, car) => {
  let direction = car.direction;

  switch (field) {
    case '+':
      turn = car.turn % 3 === 2 ? +1 : car.turn % 3 === 0 ? -1 : 0;
      direction = getDirection(car.direction, turn);
      break;
    case '/':
      direction = '<>'.includes(car.direction)
        ? getDirection(car.direction, -1)
        : getDirection(car.direction, +1);
      break;
    case '\\':
      direction = '<>'.includes(car.direction)
        ? getDirection(car.direction, +1)
        : getDirection(car.direction, -1);
      break;

    default:
      break;
  }

  return direction;
};

const moveCar = (car, map) => {
  x = car.x;
  y = car.y;
  switch (car.direction) {
    case '<':
      x = car.x - 1;
      break;
    case '>':
      x = car.x + 1;
      break;

    case 'v':
      y = car.y + 1;
      break;

    case '^':
      y = car.y - 1;
      break;
    default:
      break;
  }

  collisionCheck(car);
  return {x, y};
};

let tick = 0;
let crashCars = [];

while (cars.length > 1) {
  cars = cars
    .sort((a, b) => {
      if (a.y === b.y) {
        return a.x - b.x;
      }
      return a.y - b.y;
    })
    .map(car => {
      const {x, y} = moveCar(car, map);

      car.x = x;
      car.y = y;

      const field = map[y][x];

      car.direction = getNewDirection(field, car);
      if (field === '+') {
        car.turn++;
      }

      return car;
    })
    .filter(c => !crashCars.includes(c.id));
  tick++;
}
console.log({cars, crashCars});
