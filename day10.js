let input = require('./day10-input');

const render = (data, bounds) => {
  console.log(bounds);

  for (let y = 0; y <= bounds.y2; y++) {
    line = [];
    for (let x = 0; x <= bounds.x2; x++) {
      if (
        data.find(p => {
          return p[0][0] === x && p[0][1] === y;
        })
      ) {
        line.push('X');
      } else {
        line.push('-');
      }
    }
    console.log(line.join(''));
  }
};

const move = data => {
  return data.map(pixel => {
    const [x, y] = pixel[0];
    const [moveX, moveY] = pixel[1];

    pixel[0] = [x + moveX, y + moveY];
    return pixel;
  });
};

const getBounds = data => {
  return data.reduce(
    (b, pixel) => {
      const [x, y] = pixel[0];

      b = {
        x1: Math.min(x, b.x1),
        y1: Math.min(y, b.y1),
        x2: Math.max(x, b.x2),
        y2: Math.max(y, b.y2)
      };

      return b;
    },
    {x1: 0, y1: 0, x2: 0, y2: 0}
  );
};

const checkNext = data => {
  return data.map(pixel => {
    const [x, y] = pixel[0];
    const top = [x, y + 1];
    const right = [x + 1, y];
    return hasNext;
  });
};

console.log(getBounds(input));

let field = input;
let bounds = [];
let count = 0;
while (count < 100000) {
  field = move(field);
  const b = getBounds(field);
  // console.log(b);

  // console.log((b.x2 - b.x1) * (b.y2 - b.y1));

  bounds.push((b.x2 - b.x1) * (b.y2 - b.y1));

  if ((b.x2 - b.x1) * (b.y2 - b.y1) === 23718) {
    console.log(count);
    return;
  }

  count++;
}

// console.log(bounds.sort((a, b) => a - b));
