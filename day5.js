const input = require('./day5-input');

let length = 0;
let match = false;
const reducer = array => {
  output = array.reduce((result, l, i, a) => {
    if (match === true) {
      match = false;
      return result;
    }
    if (
      i < a.length - 1 &&
      l.toUpperCase() === a[i + 1].toUpperCase() &&
      l !== a[i + 1]
    ) {
      match = true;
      return result;
    }

    result.push(l);
    match = false;
    return result;
  }, []);

  if (length !== output.length) {
    length = output.length;

    reducer(output);
  } else {
    console.log(output.length);
  }
};

// reducer(input.split(''));

[
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z'
].forEach(l => {
  const re = new RegExp(l, 'gi');

  const p = input.replace(re, '').split('');
  console.log(l);

  reducer(p);
});
