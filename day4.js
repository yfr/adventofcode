const input = require('./day4-input');

const sorted = input
  .map(ts => {
    const time = ts.slice(1, 17);

    return {
      time,
      ts
    };
  })
  .sort((a, b) => {
    a = new Date(a.time);
    b = new Date(b.time);
    return a < b ? -1 : a > b ? 1 : 0;
  });

const result = {};

let id;
let start = null;
let index = 0;

sorted.forEach(e => {
  if (/Guard/.test(e.ts)) {
    [, , , id] = e.ts.split(' ');
  }

  if (/falls asleep/.test(e.ts)) {
    [, start] = e.time.split(':');
  }

  if (/wakes up/.test(e.ts)) {
    [, end] = e.time.split(':');
    if (!result[id]) {
      result[id] = {id};
    }

    for (let index = Number(start); index < Number(end); index++) {
      if (!result[id][index]) {
        result[id][index] = 1;
        result[id]['sum'] = 1;
      } else {
        result[id][index]++;
        result[id]['sum']++;
      }
    }
  }

  if (start && id) {
  }
});

console.log(
  Object.values(result).sort((a, b) =>
    a.sum > b.sum ? -1 : a.sum < b.sum ? 1 : 0
  )
);
