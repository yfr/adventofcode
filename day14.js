const input = '37'.split('').map(r => Number(r));
const recipesToTry = '236021';
let scoreBoard = input;
let currentRecipes = [0, 1];

const generateScoreboard = (recipes, board) => {
  const newRecipes = recipes.reduce((a, b) => a + board[b], 0).toString();

  board.push(...newRecipes.split('').map(r => Number(r)));
  return board;
};

const moveOnScoreBoard = (recipes, board) => {
  return recipes.map(r => (board.length + board[r] + 1 + r) % board.length);
};
let lastSix = 0;

while (scoreBoard.length < 3000000000) {
  if (scoreBoard.length % 1000000 === 0) {
    console.log(scoreBoard.length);
  }

  scoreBoard = generateScoreboard(currentRecipes, scoreBoard);
  currentRecipes = moveOnScoreBoard(currentRecipes, scoreBoard);
  last10 = scoreBoard.slice(-10).join('');

  if (last10.indexOf(recipesToTry) >= 1) {
    console.log(
      'WOOOO',
      scoreBoard.length - (10 - last10.indexOf(recipesToTry))
    );
    break;
  }
}

console.log(scoreBoard.length - recipesToTry.length, scoreBoard.slice(-10));
