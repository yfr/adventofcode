let input = require('./day7-input');

const start = ['B', 'F', 'U', 'X'];
const finals = [];
const finals2 = [];

let steps = [
  'A',
  'B',
  'C',
  'D',
  'E',
  'F',
  'G',
  'H',
  'I',
  'J',
  'K',
  'L',
  'M',
  'N',
  'O',
  'P',
  'Q',
  'R',
  'S',
  'T',
  'U',
  'V',
  'W',
  'X',
  'Y',
  'Z'
];

const isStart = l => {
  return !input.map(i => i[1]).includes(l);
};

const conditionsMatch = l => {
  const conditions = input.filter(i => i[1] === l).map(i => i[0]);

  return conditions.reduce((r, i) => {
    return !r ? r : finals.includes(i);
  }, true);
};

const forRealz = () => {
  steps.forEach((step, i) => {
    if ((isStart(step) || conditionsMatch(step)) && !finals.includes(step)) {
      finals.push(step);
      finals2.push(i + 1 + 60);
      // forRealz();
      return;
    }
  });
};

forRealz();

console.log(finals.join(''));
console.log(finals2.reduce((a, b) => a + b, 0));
