let input = 6303;
let maxX = 300;
let maxY = 300;

const levels = [];
let power = [];

for (let x = 0; x < maxX; x++) {
  for (let y = 0; y < maxY; y++) {
    const rackId = x + 10;

    let powerLevel = ((rackId * y + input) * rackId).toString()[
      ((rackId * y + input) * rackId).toString().length - 3
    ];
    powerLevel = powerLevel ? powerLevel - 5 : 0 - 5;

    levels.push(Number(powerLevel));
  }
}

const getPower = (x, s) => {
  let power = levels.slice(x, x + s).reduce((a, b) => a + b, 0);

  for (let index = 1; index < s; index++) {
    let i = x + 300 * index;
    let t = levels.slice(i, i + s).reduce((a, b) => a + b, 0);

    power += t;
  }

  return power;
};

for (let square = 1; square <= 300; square++) {
  for (let x = 0; x < maxX * maxX; x++) {
    const coords = [x % 300, Math.floor((x + 300) / 300) - 1, square];

    if (coords[0] <= maxX - square && coords[1] <= maxY - square) {
      power.push({
        coords,
        power: getPower(x, square)
      });
    }
  }
}

console.log(power.sort((a, b) => b.power - a.power));
